from random import randint


def generate():
    data = {}
    for i in range(500):
        data[i] = {}
        for j in range(50000):
            if randint(1, 10) == 1:
                data[i][j] = randint(1, 5)
    return data