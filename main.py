import math

import Generate


def pos(x):
    return math.sqrt(x ** 2)


print("-> Generating a fake Dataset of Users / Posts")

data = Generate.generate()

print("-> Recovering the First User as a Target")

user = data[0]
data.pop(0)
print(user)

commons = {}

print("-> Calculation of  similar Users...")

for i in data.keys():
    commons[i] = 0
    loop = 0
    for j in data[i].keys():
        if user.get(j):
            commons[i] += 1 / (1 + pos(user[j] - data[i][j]))
            loop += 1
    if loop != 0:
        commons[i] = commons[i] / loop

similar_users = sorted(commons.items(), key=lambda x: x[1])
similar_users.reverse()
similar_users = similar_users[0:250]

print("-> Calculation of the bests Posts of the most similar Users...")

similar_posts = {}

for i in range(len(similar_users)):
    u = data[similar_users[i][0]]
    posts = sorted(u.items(), key=lambda x: x[1])
    for p in posts:
        if not user.get(p[0]):
            if not similar_posts.get(p[0]):
                similar_posts[p[0]] = [0, 0]
            similar_posts[p[0]][0] += p[1]
            similar_posts[p[0]][1] += 1

for p in similar_posts:
    similar_posts[p] = similar_posts[p][0] / similar_posts[p][1]

print("-> Deduction of the bests Posts of the Users...")

similar_posts = sorted(similar_posts.items(), key=lambda x: x[1])
similar_posts.reverse()
print(similar_posts[0:5])
